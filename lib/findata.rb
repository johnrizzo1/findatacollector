require "findata/version"
require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'sqlite3'

module Findata
  class Gather
    URL            = 'https://en.wikipedia.org/wiki/List_of_S%26P_500_companies'
    SUBDIR         = 'data-hold'
    DBNAME         = "#{SUBDIR}/sp500-data.sqlite"
    YAHOO_DATA_DIR = "#{SUBDIR}/yahoo-data"
    START_DATE     = ['01', '01', '2008']
    END_DATE       = ['01', '01', '2016']
    YURL           = "https://ichart.finance.yahoo.com/table.csv?a=#{START_DATE[0]}&b=#{START_DATE[1]}&c=#{START_DATE[2]}&d=#{END_DATE[0]}&e=#{END_DATE[1]}&f=#{END_DATE[2]}&g=d&ignore=.csv&s="
    C_FIELDS       = %w(ticker_symbol security gics_sector gics_sub_industry city state date_first_added cik)
    S_FIELDS       = %w(date open high low close volume adj_close) # closing_price

    def gather_sp500()
      Dir.mkdir(SUBDIR) unless File.exists? SUBDIR
      db = SQLite3::Database.new(DBNAME)

      db.execute("DROP TABLE IF EXISTS companies;")
      db.execute("CREATE TABLE companies(id INTEGER PRIMARY KEY AUTOINCREMENT, ticker_symbol, security, gics_sector, gics_sub_industry, city, state, date_first_added, cik)")

      rows = Nokogiri::HTML(open(URL, { ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE })).css('table.sortable:first-of-type tr')[1..-1]
      puts "Number of rows: #{rows.length}"
      rows.each do |row|
        tds = row.css('td').map { |td| td.text.strip }
        puts tds[5]
        city, state = tds[5].split(', ')
        db.execute("INSERT INTO companies(ticker_symbol, security, gics_sector, gics_sub_industry, city, state, date_first_added, cik) VALUES(?, ?, ?, ?, ?, ?, ?, ?)",
                   tds[0], tds[1], tds[3], tds[4], city, state, tds[6], tds[7])
      end

      db.execute "CREATE INDEX ticker_symbol_co_cidx ON companies(ticker_symbol)"
      db.execute "CREATE INDEX security_co_idx ON companies(security)"
      db.execute "CREATE INDEX gics_sector_co_idx ON companies(gics_sector)"
      db.execute "CREATE INDEX gics_sub_industry_co_idx ON companies(gics_sub_industry)"
      db.execute "CREATE INDEX city_co_idx ON companies(city)"
      db.execute "CREATE INDEX state_co_idx ON companies(state)"
      db.execute "CREATE INDEX cik_co_idx ON companies(cik)"
    end

    def gather_historical_stock_data()
      db = SQLite3::Database.new(DBNAME)

      Dir.mkdir(YAHOO_DATA_DIR) unless File.exists? YAHOO_DATA_DIR

      db.execute("SELECT DISTINCT ticker_symbol from companies").each do |sym|
        fname = "#{YAHOO_DATA_DIR}/#{sym[0]}.csv"
        unless File.exists? fname
          puts fname
          begin
            d = open("#{YURL}#{sym[0]}", { ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE })
            File.open(fname, 'w') do |ofile|
              ofile.write(d.read)
              sleep(1.5 + rand)
            end
          rescue
            puts "unable to get data for #{sym[0]}, URI[#{YURL}#{sym[0]}]"
          end
        end
      end
    end

    def import_historical_stock_data()
      db = SQLite3::Database.new(DBNAME)

      db.execute("DROP TABLE IF EXISTS stock_prices;")
      db.execute("DROP TABLE IF EXISTS companies_and_stocks;")

      db.execute("CREATE TABLE stock_prices(#{S_FIELDS.map { |f| f=~/date/ ? f : "#{f} NUMERIC" }.join(',')}, company_id INTEGER)")
      db.execute("CREATE TABLE companies_and_stocks(#{(C_FIELDS+S_FIELDS).map { |f|
                   f =~ /date|security|ticker_symbol|gics_sector|gics_sub_industry|city|state|date_first_added|cik/ ? f : "#{f} NUMERIC" }.join(',')})")

      ## Make company ID hash for faster reference
      ## companies table must already exist
      co_ids = db.execute("SELECT id, ticker_symbol FROM companies GROUP BY ticker_symbol").inject({}) do |hsh, c|
        hsh[c[1].to_s] = c[0].to_i
        hsh
      end

      s_insert_sql   = "INSERT INTO stock_prices VALUES(#{(S_FIELDS.length+1).times.map { '?' }.join(',')})"
      cns_insert_sql = "INSERT INTO companies_and_stocks(#{(C_FIELDS+S_FIELDS).join(',')}) VALUES(#{(C_FIELDS+S_FIELDS).map { '?' }.join(',')})"

      sp_stmt  = db.prepare(s_insert_sql)
      cns_stmt = db.prepare(cns_insert_sql)

      ## Build out tables
      db.execute("SELECT DISTINCT ticker_symbol from companies").each do |sym|
        fname = "#{YAHOO_DATA_DIR}/#{sym[0]}.csv"
        puts fname
        co_id   = co_ids[sym[0].to_s]
        co_data = db.execute("SELECT #{C_FIELDS.join(',')} from companies where ticker_symbol = ?", sym[0])

        File.open(fname, 'r') do |csv|
          csv.readlines[1..-1].map { |r| r.strip.split(',') }.each do |cols|
            sp_stmt.execute(cols, co_id)
            cns_stmt.execute(co_data, cols)
          end
        end
      end

      # Create indicies for faster queries
      db.execute "CREATE INDEX company_id_index ON stock_prices(company_id)"
      db.execute "CREATE INDEX date_index ON stock_prices(date)"
      db.execute "CREATE INDEX security_idx ON companies_and_stocks(security)"
      db.execute "CREATE INDEX ticker_idx ON companies_and_stocks(ticker_symbol)"
      db.execute "CREATE INDEX city_idx ON companies_and_stocks(city)"
      db.execute "CREATE INDEX state_idx ON companies_and_stocks(state)"
      db.execute "CREATE INDEX gics_sector_idx ON companies_and_stocks(gics_sector)"
      db.execute "CREATE INDEX gics_sub_industry_idx ON companies_and_stocks(gics_sub_industry)"
      db.execute "CREATE INDEX date_idx ON companies_and_stocks(date)"
    end
  end
end
